# Generated by Django 3.2 on 2022-05-19 17:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('market', '0004_auto_20220519_2032'),
    ]

    operations = [
        migrations.AddField(
            model_name='wallpaper',
            name='name',
            field=models.CharField(default='Name', max_length=50),
            preserve_default=False,
        ),
    ]
